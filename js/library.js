/**
 * @author Markus
 */




function webGLSupported() {
    try {
        return !! window.WebGLRenderingContext && !! document.createElement( 'canvas' ).getContext( 'experimental-webgl' );
    } catch( e ) {
        return false;
    }
} 
var webgl = webGLSupported(); 

function div(x,y){
    return Math.floor(x/y);
}




//Taken from: http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
//will bind requestAnimationFrame for me
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelRequestAnimationFrame = window[vendors[x]+
          'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());




/**
 * Rotation matrices
 */

function rotationMatrixXY(){
    var m = new THREE.Matrix3();
    for (var i=0; i<9; i++) m.elements[i] = (i==1)?1 : (i==3||i==8) ?-1:0;
    return m;
}


function rotationMatrixXZ(){
    var m = new THREE.Matrix3();
    for (var i=0; i<9; i++) m.elements[i] = (i==2)?-1 : (i==4||i==6) ?1:0;
    return m;
}


/**
 * JQuery plugin to center an element - our windows 
 */
jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}


/**
 * Round x to n numbers 
 * Example: runde(0.12345,3) = 0.123
 * @param {Object} x the number to round
 * @param {Object} n the count of numbers you'll receive after the dot
 */
function runde(x, n) {
  if (n < 1 || n > 14) return false;
  var e = Math.pow(10, n);
  var k = (Math.round(x * e) / e).toString();
  return k;
}


//http://stackoverflow.com/questions/2101259/jquery-js-get-text-from-field-on-keyup-but-with-delay-for-further-typing
/**
 * Binds an onkeyup event, so it will only be fired if the user stopped typing - after 750ms without keystroke
 * @param {Object} $ JQuery object
 */
(function($){

$.widget("ui.onDelayedKeyup", {

    _init : function() {
        var self = this;
        $(this.element).keyup(function() {
            if(typeof(window['inputTimeout']) != "undefined"){
                window.clearTimeout(inputTimeout);
            }  
            var handler = self.options.handler;
            window['inputTimeout'] = window.setTimeout(function() {
                handler.call(self.element) }, self.options.delay);
        });
    },
    options: {
        handler: $.noop(),
        delay: 750
    }

});
})(jQuery);


/**
 * Returns true if two vectors are equal, otherwise false 
 */
function vector3Equal(v1,v2){
	return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
}
