/**
 * Creates a new formula element containing all important things to edit a formula
 */
function FormulaElement(graph,c){
    //===Elements===
    // - label
    // - input
    // - colorbox
    // - deleteButton
    // - dropdown/controls
    // Functions: removeSelf
    var _formelem = this;
    var makeColorClick = function(cf){
        return function(){
            _formelem.colorSel.style.backgroundImage = "";
            cf.preview(_formelem.colorSel);
            graph.setColorFunction(cf);
        }        
    }
    
    //make label    
    this.label = document.createElement("p");
    this.label.id = "lbl"+c;
    this.label.className = "label";
            
    //make a delete button
    this.deleteButton = document.createElement("div");
    this.deleteButton.className = "deleteButton";
    this.deleteButton.appendChild(document.createTextNode("X"));
    this.deleteButton.onclick = function(){  gui.removeFormula(graph);  };
    this.label.appendChild(this.deleteButton);
    
    //make the caption
    this.label.appendChild(document.createTextNode("Formula "+c));
    
    //make COLORPICKER
    this.color = document.createElement("div");
    this.color.className = "colorfktpicker";
    this.colorSel = document.createElement("div");
    this.colorSel.className = "selectedcol";
    graph.colorFunction.preview(this.colorSel);
    this.color.appendChild(this.colorSel);
    
    //add colorfunctions
    for (var i = 0; i<ColorFunctions.list.length; i++){
        var d = document.createElement("div");
        d.className = "col";
        ColorFunctions.list[i].preview(d);
        d.onclick = makeColorClick(ColorFunctions.list[i]);
        this.color.appendChild(d);
    }
    this.label.appendChild(this.color);
    
    //make formula field
    this.input = document.createElement("input");
    this.input.id = "formula"+c;
    this.input.className = "formulaInput";
    $(this.input).change(  function(){ gui.functionTermChange(graph); }  );
    $(this.input).onDelayedKeyup({
        handler: function() {
            gui.functionTermChange(graph);
        }
    }); 
    if (graph.term !== undefined) this.input.value = graph.term;
    this.label.appendChild(this.input);
    
    //dropdown area
    this.area = document.createElement("div");
    this.area.className = "dropdown";
    this.label.appendChild(this.area);
    
    //make buttons
    	//surface
    this.btn1 = document.createElement("div");
    this.btn1.className = "btn";
    this.btn1.innerHTML = "&Delta;";
    this.btn1.title = "Show/hide graph surface";
    this.btn1.onclick = function(){ graph.setVisible(!graph.visible, graph.visibleWireframe); }
    	//wireframe
    this.btn2 = document.createElement("div");
    this.btn2.className = "btn";
    this.btn2.innerHTML = "#";
    this.btn2.title = "Show/hide wireframe"
    this.btn2.onclick = function(){ graph.setVisible(graph.visible, !graph.visibleWireframe); }
    	//transparency
    this.btn3 = document.createElement("div");
    this.btn3.className = "btn";
    this.btn3.innerHTML = "&alpha;";
    this.btn3.style.fontFamily = "arial helvetica sans-serif";
    this.btn3.style.fontStyle = "italic";
    this.btn3.title = "Enable/disable transparency";
    this.btn3.onclick = function(){ graph.setTransparent(!graph.material.transparent); }
    	//resolution decrease
    this.btn4 = document.createElement("div");
    this.btn4.className = "btn";
    this.btn4.style.marginLeft = "10px";
    this.btn4.innerHTML = "R&darr;";
    this.btn4.title = "Decrease resolution for this graph";
    this.btn4.onclick = function(){ 
        graph.detailsX = Math.round(graph.detailsX / 1.5); 
        graph.detailsY = Math.round(graph.detailsY / 1.5); 
        graph.detailsXr = graph.detailsXr / 1.5; 
        graph.detailsYr = graph.detailsYr / 1.5; 
        graph.renderGraph();
        if (graph == gui.intersect1 || graph == gui.intersect2){
            gui.updateIntersection();
        }
    }
    	//resolution increase
    this.btn5 = document.createElement("div");
    this.btn5.className = "btn";
    this.btn5.innerHTML = "R&uarr;";
    this.btn5.title = "Increase resolution for this graph";
    this.btn5.onclick = function(){ 
        if (graph.detailsX * 1.5 * graph.detailsY * 1.5 > facesBound){
            if (!gui.askTooManyFaces(Math.round(graph.detailsX * 1.5 * graph.detailsY * 1.5))) return;
        } 
        graph.detailsX = Math.round(graph.detailsX * 1.5); 
        graph.detailsY = Math.round(graph.detailsY * 1.5);
        graph.detailsXr = graph.detailsXr * 1.5; 
        graph.detailsYr = graph.detailsYr * 1.5; 
        graph.renderGraph();
        if (graph == gui.intersect1 || graph == gui.intersect2){
            gui.updateIntersection();
        }
    } 
        //Render area
    var tab = document.createElement("table");
    tab.className = "areaTable";
    tab.innerHTML = '<tr><td rowSpan=2>Draw <br>from </td><td><input></td></tr><tr><td><input></td></tr>';
    var tab2 = document.createElement("table");
    tab2.className = "areaTable";
    tab2.innerHTML = '<tr><td rowSpan=2>to </td><td><input></td></tr><tr><td><input></td></tr>';
    this.fromX = tab.getElementsByTagName("input")[0];
    this.fromY = tab.getElementsByTagName("input")[1];
    this.toX   = tab2.getElementsByTagName("input")[0];
    this.toY   = tab2.getElementsByTagName("input")[1];
    this.fromX.className = "areaInput";
    this.fromY.className = "areaInput";
    this.toX.className = "areaInput";
    this.toY.className = "areaInput";
    this.fromX.value = graph.drawFromX;
    this.fromY.value = graph.drawFromY;
    this.toX.value   = graph.drawToX;
    this.toY.value   = graph.drawToY;
    var MyNumber = function(s){
        if (s === ""){ return NaN; }else{ return Number(s); }
    }
    var f = function(){
        var fx = MyNumber(graph.gui.fromX.value);
        var fy = MyNumber(graph.gui.fromY.value);
        var tx = MyNumber(graph.gui.toX.value);
        var ty = MyNumber(graph.gui.toY.value);
        if (!isFinite(fx)) fx = graph.drawFromX;
        if (!isFinite(fy)) fy = graph.drawFromY;
        if (!isFinite(tx)) tx = graph.drawToX;
        if (!isFinite(ty)) ty = graph.drawToY;
        graph.setDrawingArea(fx,fy,tx,ty, false);
        gui.updateArea();
    }
    $(this.fromX).change(f);
    $(this.fromY).change(f);
    $(this.toX).change(f);
    $(this.toY).change(f);
    $(this.fromX).onDelayedKeyup({handler: f});
    $(this.fromY).onDelayedKeyup({handler: f});
    $(this.toX).onDelayedKeyup({handler: f});
    $(this.toY).onDelayedKeyup({handler: f});
    

    this.area.appendChild(this.btn1);
    this.area.appendChild(this.btn2);
    this.area.appendChild(this.btn3);
    this.area.appendChild(this.btn4);
    this.area.appendChild(this.btn5);
    this.area.appendChild(document.createElement("br"));
    this.area.appendChild(tab);
    this.area.appendChild(tab2);
    
    //add to container 
    document.getElementById("formulas").appendChild(this.label);
    $(this.label).click(function(){ gui.selectFormula(graph); });
    this.input.focus();
    graph.gui = this;
    
    this.removeSelf = function(){
        $(this.label).remove(); //I start to like JQuery :D
    }
}

/**
 * Creates a new parameter slider 
 */
function ParamSlider(param){
	var _slider = this;
	this.param = param;
	graphset.updateParameter(param,0);
	this.div = document.createElement("div");
	this.div.className = "paramslider";
	this.slider = document.createElement("div");
	$(_slider.slider).slider({
			value: 0,
			min: -10,
			max: 10,
			step: 0.1,
			slide: function( event, ui ) {
				_slider.inputval.value = ui.value;
				graphset.updateParameter(param,ui.value);
			}
		});
	
	
	this.inputmin = document.createElement("input");
	this.inputmin.className="inputMin";
	this.inputmax = document.createElement("input");
	this.inputmax.className="inputMax";
	this.inputval = document.createElement("input");
	this.inputval.className="inputVal";
	var fv = function(){
		var x = Number(_slider.inputval.value);
		if (x === NaN) return;
		$(_slider.slider).slider("option", "value", x);
		graphset.updateParameter(param, x);
	}
	var fm = function(){
		$(_slider.slider).slider("option", "min", Number(_slider.inputmin.value));
		$(_slider.slider).slider("option", "max", Number(_slider.inputmax.value));
		$(_slider.slider).slider("option", "step", Math.pow(10, Math.floor(Math.log(_slider.inputmax.value-_slider.inputmin.value)/Math.LN10)-2)); //:D
		_slider.inputval.value = $(_slider.slider).slider("option", "value");
	}
	var option = $(_slider.slider).slider("option");
	this.inputval.value = option.value;
	this.inputmin.value = option.min;
	this.inputmax.value = option.max;
	$(this.inputval).change(fv);
    $(this.inputval).keyup(fv);
    $(this.inputmin).change(fm);
    $(this.inputmin).keyup(fm);
    $(this.inputmax).change(fm);
    $(this.inputmax).keyup(fm);
	
	this.caption = document.createElement("h3");
	this.caption.appendChild(this.inputmin);
	this.caption.appendChild(this.inputmax);
	this.caption.appendChild(document.createTextNode(param+" = "));
	this.caption.appendChild(this.inputval);
	this.caption.style.textAlign = "center";	
	
	this.div.appendChild(this.caption);
	this.div.appendChild(this.slider);
	
	this.remove = function(){
		$(_slider.div).remove();
	}
}

/**
 *Tests if using on a touch device 
 */
function is_touch_device() {
	return !!('ontouchstart' in window) // works on most browsers 
    	|| !!('onmsgesturechange' in window); // works on ie10
};


function Gui() {
	
    var _gui = this;
    var counter = 0;
    this.container = document.getElementById("container3d");

    /**
     * Shows a warning that WebGL is not available and you use the slow Canvas renderer
     */
    this.showWebGLWarning = function() {
        //window.alert("Sorry, but it seems your browser does not support WebGL, so a slower 3d renderer will be used instead. If your are on Android, try Firefox Mobile.");
        document.getElementById("warningmessage").style.display = "block";
    }
    
    /**
     * Shows grid
     */
    this.showGrid = function() {
    	engine.coordinateGrid.setVisible(!engine.coordinateGrid.visible);
        document.getElementById('btnBox').className = engine.coordinateGrid.visible ? 'btnBox' : 'btnBoxOff';
    }
    
    /**
     * Shows axes
     */
    this.showAxes = function() {
        engine.axes.setVisible(!engine.axes.visible);
        document.getElementById('btnAxis').className = engine.axes.visible ? 'btnAxis' : 'btnAxisOff';
    }
    
    /**
     * Render function when changed
     */
    this.functionTermChange = function(graph) {
        if (graph.term == graph.gui.input.value) return;
        var res = graph.setTerm(graph.gui.input.value);
        graph.gui.input.style.backgroundColor = res===true ? "#ffffff" : "#ffa0a0";
        graph.gui.input.title = res !== true ? res : "";
        if (res !== true) return;
        _gui.refreshParams();
        graph.renderGraph();
        if (graph == _gui.intersect1 || graph == _gui.intersect2){
        	_gui.updateIntersection();
        }
        if (graph == graphset.selected){
            graphset.select(null);
        }
    }

    /**
     * If a function would create too many faces a warning message will be displayed
     */
    this.askTooManyFaces = function(count){
        return window.confirm('This calculation is going to create '+count+' faces, this could take really long. Continue?');
    }
    
    
    /**
     * Adds a new formula field and the corresponding graph. Term is optional
     */
    this.addFormula = function(term) {
    	counter += 1;
    	c = counter.toString();
    	var g = new Graph();  //create the new function
    	g.setColorFunction(ColorFunctions.list[(c-2) % ColorFunctions.list.length]);
    	graphset.add(g); 
    	new FormulaElement(g,c);
    	gui.selectFormula(g);
    	if (term === undefined) return;
    	g.gui.input.value = term;
    	gui.functionTermChange(g);
    }
    
    this.addFormulaGraph = function(graph){
        counter += 1;
        c = counter.toString();
        graphset.add(graph, false); 
        new FormulaElement(graph,c);
        gui.selectFormula(graph);
    }
    
    
    /**
     * Removes a formula field. It gets passed the according graph, because a graph has now a .gui element
     */
    this.removeFormula = function(graph) {
    	//you want to destruct an element? Do it right - delete graph and gui element
    	if (graph === _gui.intersect1 || graph === _gui.intersect2) _gui.removeIntersection();
    	graphset.remove(graph);        //delete from list
    	graph.remove();                //remove from 3d scene
    	graph.gui.removeSelf();        //remove from gui
    	_gui.refreshParams();
    }
    
    
    this.selectFormula = function(graph){
        for (var i=0; i<graphset.graphs.length; i++){
            graphset.graphs[i].gui.label.className = graphset.graphs[i] === graph ? "selected" : "";
        }
    }
    
    
    
    this.updateArea = function(){
    	if (graphset.graphs.length == 0) return;
    	//get maximum
    	var fx = graphset.graphs[0].drawFromX;
    	var fy = graphset.graphs[0].drawFromY;
    	var tx = graphset.graphs[0].drawToX;
    	var ty = graphset.graphs[0].drawToY;
    	var m = -1, M = 1;
    	for (var i=1; i< graphset.graphs.length; i++){
    		fx = Math.min(fx, graphset.graphs[i].drawFromX);
    		fy = Math.min(fy, graphset.graphs[i].drawFromY);
    		tx = Math.max(tx, graphset.graphs[i].drawToX);
    		ty = Math.max(ty, graphset.graphs[i].drawToY);
    		if (isFinite(graphset.graphs[i].minValue)) m = Math.min(m, graphset.graphs[i].minValue);
    		if (isFinite(graphset.graphs[i].maxValue)) M = Math.max(M, graphset.graphs[i].maxValue);
    	}
    	fx = Math.floor(fx);
    	fy = Math.floor(fy);
    	tx = Math.ceil(tx);
    	ty = Math.ceil(ty);
    	m  = Math.floor(m);
    	M  = Math.ceil(M);
    	engine.coordinateGrid.update(new THREE.Vector3(fx-1,fy-1,m-1)    , new THREE.Vector3(tx+1,ty+1,M+1) , 
    	                             new THREE.Vector3(fx-50,fy-50,m-50) , new THREE.Vector3(tx+50,ty+50,M+50));
    	fx = Math.min(fx, -1);
    	fy = Math.min(fy, -1);
    	tx = Math.max(tx,  1);
    	ty = Math.max(ty,  1);
    	m  = Math.min(m,  -1);
    	M  = Math.max(M,   1);
    	engine.axes.update(new THREE.Vector3(fx,fy,m) , new THREE.Vector3(tx,ty,M));
    }
    
    
    
    // === SLIDER ===
    this.slider = document.getElementById("slider-vertical");
    var _slider = this.slider;
    //Zoom slider
		$( _slider ).slider({
			orientation: "vertical",
			range: "min",
			min: 0,
			max: 100,
			value: 50,
			slide: function( event, ui ) {
				engine.controls1.sliderZoom(engine.controls1.sliderToZoom(ui.value));
			}
		});
	engine.controls1.onzoom = function(difference){
		var f = engine.controls1.zoomToSlider(difference);
		$(_slider).slider( "option", "value", f); 
	}


	// TOUCH stuff
	// special support for touch enabled devices
   	if (is_touch_device()) {
   		$(function() {
			$('#addMore').click( function(ev) { onclick; } );
			$('#deleteButton').click( function(ev) { onclick; } );
			$('#btnReset').click( function(ev) { onclick; } );
			$('#btnHighres').click( function(ev) { onclick; } );
			$('#btnIntersect').click( function(ev) { onclick; } );
			$('#btnAxis').click( function(ev) { onclick; } );
			$('#btnBox').click( function(ev) { onclick; } );
			$('#btnSave').click( function(ev) { onclick; } );
			$('#btnHelp').click( function(ev) { onclick; } );
			$('#buttons').click( function(ev) { onclick; } );
			$('#deleteButton').click( function(ev) { onclick; } );
    	});
    	
	   	var hammer00 = new Hammer(document.getElementById("container3d"));
		
		hammer00.ondoubletap = function(ev) { 
			ev.clientX = ev.position[0].x
			ev.clientY = ev.position[0].y 
			engine.controls2.mouseDblClick(ev);
		};
		
		hammer00.ontap = function(ev) {
			ev.clientX = ev.position[0].x
			ev.clientY = ev.position[0].y
			engine.controls2.mouseClick(ev); 
		};
	}
	
            		
	// === SCREENSHOT window ===
	this.showScreenshotWindow = function(){
		var div = _gui.makeWindow("Save as...", "centeredWindow");
		_gui.makeWindowText(div, "As URL: ");
		var urlfield = document.createElement("input");
		urlfield.readOnly = "readonly";
		urlfield.value = Serializer.getURL();
		urlfield.className = "urlfield";
		$(urlfield).focus(function(){ urlfield.select(); });
		div.appendChild(urlfield);
		_gui.makeWindowText(div, 'Right-click on the picture and choose "Save As..."');
		var iframe = document.createElement("iframe");
		iframe.src = engine.renderer.domElement.toDataURL("image/png");
		//iframe.width = "200";
		try{
			iframe.scrolling = "no";
		}catch(e){}
		div.appendChild(iframe);
	}
	
	
	// === HELP window ===
	this.showHelpWindow = function(){
		var div = _gui.makeWindow("Mathematical functions", "centeredWindowHelp");
		var iframe = document.createElement("iframe");
		iframe.src = "help.html";
		try {
			iframe.scrolling = "auto";
		} catch(e) {}
		div.appendChild(iframe);
	}
	
	this.makeWindowText = function(windowDiv,t){
		var text = document.createElement("div");
		text.className = "textfield";
		text.appendChild(document.createTextNode(t));
		windowDiv.appendChild(text);
		return text;
	}	
	this.makeWindow = function(title, className){
		var h1 = document.createElement("h1");
		var closeButton = document.createElement("div");		
		var div = document.createElement("div");
		div.className = className;
		h1.appendChild(document.createTextNode(title));
		closeButton.className = "deleteButtonBig";
    	closeButton.appendChild(document.createTextNode("X"));
    	closeButton.onclick = function(){  $(div).remove();  };
		div.appendChild(closeButton);
		div.appendChild(h1);
		$(div).draggable();
		$(div).center();
		document.body.insertBefore(div, document.body.firstChild);
		return div;
	}
		
	this.renderHighRes = function() {
	    if (graphset.selected === null){
	        alert("Please select a point on the graph first!");
	        return;
	    }
		var bla = graphset.selected.selEvent;
		graphset.selected.dblClick(bla);		
	}
	
	
	this.intersect1 = this.intersect2 = null;
	this.intersectMaterial = new THREE.MeshBasicMaterial({color: 0xff0000, transparent: true, opacity: 0.9});
	this.intersectMesh = null;
	this.updateIntersection = function(){
		if (_gui.intersectMesh !== null) engine.scene.remove(_gui.intersectMesh);
		var geo = calculateCuttingGeometry(_gui.intersect1,_gui.intersect2);
		_gui.intersectMesh = new THREE.Mesh(geo, _gui.intersectMaterial);
		engine.scene.add(_gui.intersectMesh);
	}
	
	this.removeIntersection = function(){
	    _gui.intersect1.setTransparent(false);
        _gui.intersect2.setTransparent(false);
        _gui.intersect1 = _gui.intersect2 = null;
        engine.scene.remove(_gui.intersectMesh);
        _gui.intersectMesh = null;
	}
		
	this.showIntersection = function(){
		if (_gui.intersect1 !== null && _gui.intersect2 !== null){
			_gui.removeIntersection();
			return;
		}
		_gui.intersect1 = _gui.intersect2 = null;
		
		if (graphset.graphs.length == 2){
		    _gui.intersect1 = graphset.graphs[0];
		    _gui.intersect2 = graphset.graphs[1];
		    _gui.intersect1.setTransparent(true);
		    _gui.intersect2.setTransparent(true);
            _gui.updateIntersection();
            return;
		}
		
		//onclick event
		var makefkt = function(g,elem){
			return function(){
			    if (g.parsedTerm === null) return;
				if (elem.className == 'intersectionFormula_selected'){
					_gui.intersect1 = null;
					elem.className = 'intersectionFormula';
					return;
				}
				if (_gui.intersect1 === null){
					_gui.intersect1 = g;
					elem.className = 'intersectionFormula_selected';
				}else{
					_gui.intersect2 = g;
					_gui.intersect1.setTransparent(true);
					_gui.intersect2.setTransparent(true);
					_gui.updateIntersection();
					$(div).remove();
				}
			}			
		};
		
		//make window
		var div = _gui.makeWindow("Intersection", "centeredWindowIntersect");
		_gui.makeWindowText(div, 'Select two functions to calculate their intersection.');
		var c1 = document.createElement("div");
		var c2 = document.createElement("div");
		c1.id = "intersectionC1";
		c2.id = "intersectionC2";
		div.appendChild(c1);
		div.appendChild(c2);
		for (var i=0; i < graphset.graphs.length; i++){
			var elem = document.createElement('div');
			elem.className = 'intersectionFormula';
			elemTxt = document.createElement('span');
			elemTxt.appendChild(document.createTextNode(graphset.graphs[i].term));
			elemTxt.className = 'formulaInput';
			elem.appendChild(elemTxt);
			$(elem).click(makefkt(graphset.graphs[i], elem));
			if (i < graphset.graphs.length/2){
				c1.appendChild(elem);
			}else{
				c2.appendChild(elem);
			}
		}
	}
	
	
	//=== Parameter ===
	this.params = [];
	this.paramContainer = document.getElementById("parameters");
	
	/**
	 * Goes through the formulas and checks for all needed parameters. 
	 * Should be called after a formula changes.  
	 */
	this.refreshParams = function(){
		//get needed params
		var a = [];
		for (var i=0; i<graphset.graphs.length; i++){
			a = graphset.graphs[i].addParameters(a);
		}
		//remove doubles
		a.sort();
		var last = '';
		for (var i = 0; i < a.length; i++) {
      		if (last == a[i]) {
         		a.splice(i, 1);
         		i--;
      		} else {
         		last = a[i];
      		}
   		}
   		//insert/remove entries from params
   		var ip=0, ia=0;
   		while(ip < _gui.params.length && ia < a.length){
   			if (_gui.params[ip].param == a[ia]){
   				ip++; ia++; //param already exists
   			}else if(_gui.params[ip].param < a[ia]){
   				_gui.params[ip].remove();
   				_gui.params.splice(ip,1); //remove param
   			}else{
   				var p = new ParamSlider(a[ia]);
   				_gui.paramContainer.insertBefore(p.div, _gui.params[ip].div);
   				_gui.params.splice(ip,0,p);
   				ip++; ia++; //new Slider
   			}
   		}
   		//not needed sliders
   		while (ip < _gui.params.length){
   			_gui.params[ip].remove();
   			_gui.params.splice(ip,1);
   		}
   		//new params
   		while (ia < a.length){
   			var p = new ParamSlider(a[ia]);
   			_gui.paramContainer.appendChild(p.div);
   			_gui.params.push(p);
   			ia++;
   		}   			
	}
	
	
	// === DETAILS === Position/Derivation
	this.detailNone    = document.getElementById("werteNoDetails");
	this.detailTable   = document.getElementById("werte");
	this.detailX       = document.getElementById("werteX");
	this.detailY       = document.getElementById("werteY");
	this.detailZ       = document.getElementById("werteZ");
	this.detailDX      = document.getElementById("werteDX");
	this.detailDY      = document.getElementById("werteDY");
	this.detailCalc    = document.getElementById("werteCalc");
	this.detailSurface = document.getElementById("werteSurface");
	this.lastSelected  = null; //only hide the surface when switching the graph
	
	/**
	 * Shows details about a selected point. Pass null to say "no details" 
	 */
	this.setDetails = function(x,y,z,dx,dy){
	    if (x === undefined || x === null){
	        _gui.detailNone.style.display = 'block';
	        _gui.detailTable.style.display = 'none';
	        _gui.lastSelected = null;
	        return;
	    }
	    
	    _gui.detailNone.style.display = 'none';
        _gui.detailTable.style.display = 'table';
	    _gui.detailX.innerHTML  = runde(x,6);
	    _gui.detailY.innerHTML  = runde(y,6);
	    _gui.detailZ.innerHTML  = runde(z,6);
	    _gui.detailDX.innerHTML = runde(dx,6);
	    _gui.detailDY.innerHTML = runde(dy,6);
	    if (_gui.lastSelected !== graphset.selected){
	        _gui.lastSelected = graphset.selected;
	        _gui.detailSurface.style.display = "none";
	        _gui.detailCalc.disabled = false;
	        _gui.detailCalc.value = "Approximate!";
	        _gui.detailCalc.style.display = "inline";
	        //calculate
	        _gui.detailCalc.onclick = function(){
	            //disable button
	            _gui.detailCalc.disabled = true;
	            _gui.detailCalc.value = "Calculating...";
	            
	            window.setTimeout(function(){
	               //calculate
	               var s = graphset.selected.getSurface();
	               //show
	               _gui.detailSurface.innerHTML = runde(s,6);
	               _gui.detailSurface.style.display = "inline";
	               _gui.detailCalc.style.display = "none";
	           }, 50);
	        }
	    }
	}
	   
}